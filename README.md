# Django Hardware Store

Django Hardware Store is a simple Python/Django application made in order to make the hardware store employees workflow easier.

## Deployment

To deploy project:

1. Define the `DJANGO_SETTINGS_MODULE` enviroment variable (set it to either `project.settings.dev` or `project.settings.prod` according to your needs).
2. If youre are deploying prod, set the `SECRET_KEY` enviroment variable.
3. Execute `docker-compose -f deploy/docker-compose.yml build`.
4. Execute `docker-compose -f deploy/docker-compose.yml up`.
5. Visit [localhost](http://localhost).

## Testing

To run tests:

1. Build images `docker-compose -f deploy/docker-compose.yml build`
2. Execute `docker-compose -f deploy/docker-compose.yml run django poetry run python3 manage.py test`
