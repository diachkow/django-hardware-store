import os

from project.settings.base import *

SECRET_KEY = os.environ.get('SECRET_KEY')

DEBUG = False

SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
