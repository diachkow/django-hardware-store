from project.settings.base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'dhs_db',
        'USER': 'admin',
        'HOST': os.environ.get('DHS_DB_HOST', '0.0.0.0'),
        'PASSWORD': 'qwerty123',
        'PORT': '5432',
    }
}
