from decimal import Decimal

import factory

from products.models import Product


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product
        django_get_or_create = ('name',)

    name = 'Processor'
    price = Decimal(2020.20)
