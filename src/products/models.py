from django.db import models


class Product(models.Model):
    """Product django model

    Product is a thing or a service that store offers to a customer.

    Attributes:
        name (str): A product name
        price (Decimal): A product price
    """

    name = models.CharField(max_length=128)
    price = models.DecimalField(max_digits=19, decimal_places=2)
    create_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
