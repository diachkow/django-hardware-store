# Generated by Django 3.0.7 on 2020-06-12 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0003_auto_20200610_1416'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.IntegerField(choices=[(1, 'New'), (2, 'Completed'), (3, 'Paid')], default=1),
        ),
    ]
