from django.db import models

from sentry_sdk import capture_message

from orders.statuses import OrderStatus
from orders.discounts.discounters import discounter
from orders.exceptions import InvalidOrderStatusException


class ReceiptManager(models.Manager):
    """A receipt django model manager

    Provides a simple API for creating Receipt instances based on
    finilized order instances (that has PAID status). It automatically
    calculates the discount amount and substracts discount from the
    order total price, saving data whether order was discounted or not.
    """

    def create_receipt(self, order):
        if order.status != OrderStatus.COMPLETED:
            raise InvalidOrderStatusException(
                'Only completed objects can be finilized'
            )

        discount = discounter.get_discount(order)
        total_price = order.product.price - discount
        is_discounted = discount > 0

        receipt = self.model(
            order=order,
            total_price=total_price,
            is_discounted=is_discounted,
        )
        receipt.save()

        order.finilize()

        capture_message(
            f'Created new receipt on product "{order.product.name}"'
            f'(total price: {total_price}, discounted: {is_discounted})',
            level='info'
        )

        return receipt
