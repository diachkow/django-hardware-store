from django.utils import timezone

import factory

from products.factories import ProductFactory

from orders.models import Order
from orders.statuses import OrderStatus


class OrderFactory(factory.django.DjangoModelFactory):
    product = factory.SubFactory(ProductFactory)
    status = OrderStatus.NEW
    create_date = timezone.now()

    class Meta:
        model = Order
        django_get_or_create = ('product', 'create_date')
