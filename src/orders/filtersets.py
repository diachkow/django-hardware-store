import django_filters

from orders.models import Order


class OrderFilterset(django_filters.FilterSet):
    status = django_filters.NumberFilter()

    create__year__gte = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='year__gte'
    )
    create__month__gte = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='month__gte'
    )
    create__day__gte = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='day__gte'
    )
    create__day__gt = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='day__gt'
    )


    create__year__lte = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='year__lte'
    )
    create__month__lte = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='month__lte'
    )
    create__day__lte = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='day__lte'
    )
    create__day__lt = django_filters.NumberFilter(
        field_name='create_date',
        lookup_expr='day__lt'
    )

    class Name:
         model = Order
