from django.test import TestCase

from products.factories import ProductFactory

from orders.models import Order
from orders.statuses import OrderStatus
from orders.forms import OrderCompleteForm, ReceiptForm
from orders.factories import OrderFactory


class TestOrderCompleteForm(TestCase):
    def test_order_complete_form(self):
        order = OrderFactory()
        form = OrderCompleteForm({'id': order.id})

        self.assertTrue(form.is_valid())

        order = form.save()

        self.assertEqual(order.status, OrderStatus.COMPLETED)

    def test_order_complete_form_invalid_status(self):
        order = OrderFactory(status=OrderStatus.PAID)
        form = OrderCompleteForm({'id': order.id})

        self.assertFalse(form.is_valid())


class TestReceiptForm(TestCase):
    def test_receipt_form(self):
        order = OrderFactory(status=OrderStatus.COMPLETED)
        form = ReceiptForm({'id': order.id})

        self.assertTrue(form.is_valid())

        receipt = form.save()

        self.assertEqual(receipt.order.status, OrderStatus.PAID)
        self.assertIsNotNone(receipt.id)

    def test_receipt_form_invalid_order_status(self):
        order = OrderFactory(status=OrderStatus.NEW)
        form = ReceiptForm({'id': order.id})

        self.assertFalse(form.is_valid())
