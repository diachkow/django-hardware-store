from django.test import TestCase

from orders.discounts.discounters import Discounter
from orders.discounts import MonthlyProductDiscount
from orders.exceptions import DiscounterConfigError

class TestDiscounter(TestCase):
    def setUp(self):
        self.discounter = Discounter()

    def test_dicounter_config(self):
        dscntr = Discounter()

        dscntr.add_discount(MonthlyProductDiscount)
        dscntr.add_discount(MonthlyProductDiscount())

        self.assertEqual(len(dscntr.discounts), 2)
        self.assertTrue(
            isinstance(dscntr.discounts[0], MonthlyProductDiscount)
        )
        self.assertTrue(
            isinstance(dscntr.discounts[1], MonthlyProductDiscount)
        )

        with self.assertRaises(DiscounterConfigError) as context:
            dscntr.add_discount('invalid data class')
