from django.test import TestCase

from orders.models import Order, Receipt
from orders.statuses import OrderStatus
from orders.factories import OrderFactory
from orders.exceptions import InvalidOrderStatusException


class TestReceiptManager(TestCase):
    def test_receipt_manager_create_receipt(self):
        order = OrderFactory(status=OrderStatus.COMPLETED)

        receipt = Receipt.objects.create_receipt(order)

        self.assertIsNotNone(receipt.id)
        self.assertEqual(order, receipt.order)

    def test_receipt_manager_invalid_order_status(self):
        order = OrderFactory(status=OrderStatus.NEW)

        with self.assertRaises(InvalidOrderStatusException) as e:
            receipt = Receipt.objects.create_receipt(order)
