from django.contrib import admin

from orders.models import Order, Receipt


admin.site.register(Order)
admin.site.register(Receipt)
