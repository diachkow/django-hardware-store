from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, FormView
from django.views.generic.list import ListView

from rolepermissions.mixins import HasPermissionsMixin

from orders.models import Order
from orders.statuses import OrderStatus
from orders.forms import OrderForm, OrderCompleteForm, ReceiptForm
from orders.filtersets import OrderFilterset


class OrderCreateView(HasPermissionsMixin, CreateView):
    required_permission = 'can_create_order'
    form_class = OrderForm
    template_name = 'orders/create.html'
    success_url = reverse_lazy('orders:completed')


class OrderListView(HasPermissionsMixin, ListView):
    required_permission = 'can_manage_orders'
    template_name = 'orders/list.html'
    paginate_by = 25
    context_object_name = 'orders'

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(*args, **kwargs)
        context_data['status'] = {
            'NEW': OrderStatus.NEW,
            'COMPLETED': OrderStatus.COMPLETED,
            'PAID': OrderStatus.PAID
        }

        return context_data
    
    def get_queryset(self):
        return OrderFilterset(
            self.request.GET,
            queryset=Order.objects.all()
        ).qs


class NewOrderListView(HasPermissionsMixin, ListView):
    required_permission = 'can_read_new_order'
    paginate_by = 30
    template_name = 'orders/new.html'
    context_object_name = 'orders'

    def get_queryset(self):
        return Order.objects.filter(status=OrderStatus.NEW)


class CompletedOrderListView(HasPermissionsMixin, ListView):
    required_permission = 'can_read_completed_order'
    paginate_by = 30
    context_object_name = 'orders'
    template_name = 'orders/completed.html'

    def get_queryset(self):
        return Order.objects.filter(status=OrderStatus.COMPLETED)


class OrderCompleteView(HasPermissionsMixin, FormView):
    required_permission = 'can_complete_order'
    form_class = OrderCompleteForm
    success_url = reverse_lazy('orders:new')

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class ReceiptCreateView(HasPermissionsMixin, FormView):
    required_permission = 'can_create_receipt'
    form_class = ReceiptForm
    success_url = reverse_lazy('orders:completed')
    template_name = 'orders/receipt_create.html'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
