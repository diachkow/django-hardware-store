from django.db import models

from orders.managers import ReceiptManager
from orders.statuses import OrderStatus


class Order(models.Model):
    """Customer order django model

    Attributes:
        status (int): Current order status (OrderStatus enum)
        create_date (datetime): A date of order creation
        product (Product): A product, that customer wants to buy
    """
    status = models.IntegerField(
        choices=OrderStatus.choices,
        default=OrderStatus.NEW
    )
    create_date = models.DateTimeField(auto_now_add=True)
    product = models.ForeignKey(
        'products.Product',
        on_delete=models.PROTECT,
        related_name='orders'
    )

    class Meta:
        ordering = ('create_date',)

    def complete(self):
        self.status = OrderStatus.COMPLETED
        self.save()

    def finilize(self):
        self.status = OrderStatus.PAID
        self.save()


class Receipt(models.Model):
    """Order receipt django model

    Receipt is a document that stores data about successfully finished
    deal between store and customer.

    Attribtues:
        total_price (Decimal): Order total price, including discount
        is_discounted (bool): A Boolean variable that shows whether
            discount was granted to shopping
        order (Order): A order that receipt finishes
        create_date (datetime): A date of receipt creation
    """

    total_price = models.DecimalField(max_digits=19, decimal_places=2)
    create_date = models.DateTimeField(auto_now_add=True)
    is_discounted = models.BooleanField(default=False)
    order = models.OneToOneField(
        Order,
        on_delete=models.PROTECT,
        related_name='receipt'
    )

    objects = ReceiptManager()
