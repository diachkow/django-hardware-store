from django.forms import ValidationError

from orders.models import Order


def validate_order_id(order_id):
    """
    Checks if the order with given ID exists.

    Args:
        order_id (int): An ID of order to be searched

    Raises:
        ValidationError: If the order with given ID was not found

    Returns:
        int: An ID of order to be searched
    """
    query = Order.objects.filter(id=order_id)

    if not query.exists():
        raise ValidationError(
            f'An order with ID {order_id} does not exist',
            code='invalid_id'
        )

    return order_id
