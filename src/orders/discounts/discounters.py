from .base import BaseAbstractDiscount
from orders.exceptions import DiscounterConfigError


class Discounter:
    """Discount manager (applier) class

    Discounter class provides API for calculating the total amount of
    discount that should be applied to particular order.

    Attributes:
        discounts (List[AbstractBaseDiscount]): A list of
            AbstractBaseDiscount subclass instances

    Args:
        discounts (Optional[List[AbstractBaseDiscount]]): Initial list
            of discounts
    """

    def __init__(self, discounts=None):
        self.discounts = discounts or []

    def get_discount(self, order):
        """
        Returns a total sum of discounts that was applied to an order.
        """
        return sum(
            discount.get_discount_amount(order)
            for discount
            in self.discounts
        )

    def add_discount(self, discount):
        if isinstance(discount, type):
            discount = discount()

        if not isinstance(discount, BaseAbstractDiscount):
            raise DiscounterConfigError(
                'A discount argument should be AbstractBaseDiscount '
                f'subclass or its instance, not {type(discount)}'
            )

        self.discounts.append(discount)

    def register_discount_class(self):
        def wrapper(discount_class):
            self.add_discount(discount_class)
            return discount_class
        return wrapper


discounter = Discounter()
