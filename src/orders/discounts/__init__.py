from django.utils import timezone

from dateutil.relativedelta import relativedelta

from .base import BaseAbstractDiscount
from .discounters import discounter

@discounter.register_discount_class()
class MonthlyProductDiscount(BaseAbstractDiscount):
    """
    Discount for products for 20 percents that are stored in shop
    for over a month.
    """

    def get_discount_amount(self, order):
        month_ago = timezone.now() - relativedelta(months=1)

        if order.product.create_date <= month_ago:
            return order.product.price * .2

        return super().get_discount_amount(order)
