from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):
    """Custom User model manager

    In addition to common model manager methods, provides API for
    creating users and superusers with all needed params.
    """

    def create_user(self, email, first_name, last_name, password=None,
                    second_name='', role=None, save=True):
        """Creates user with given args

        Args:
            email (str): User email
            first_name (str): User's first name
            last_name (str): User's last name
            password (Optional[str]):
            second_name (Optional[str]): User's second name
            roles (Optional[str]): A string repersetntation of
                django-role-permissions created role
            save (bool): A boolean value shows whether the craeted
                instance should be saved in database or not

        Returns:
            User: A user model instance
        """

        user = self.model(
                email=email,
                first_name=first_name,
                second_name=second_name,
                last_name=last_name,
        )

        if password:
            user.set_password(password)

        if save:
            user.save(using=self._db)

        user.role = role
        return user

    def create_superuser(self, email, first_name, last_name, password=None,
                         second_name='', save=True):
        """Creates superuser with given args

        Args:
            email (str): User email
            first_name (str): User's first name
            last_name (str): User's last name
            password (Optional[str]):
            second_name (Optional[str]): User's second name
            roles (Optional[str]): A string representation of
                django-role-permission role
            save (bool): A boolean value shows whether the craeted
                instance should be saved in database or not

        Returns:
            User: A user model instance with `is_admin = True`
        """

        user = self.create_user(
                email=email,
                first_name=first_name,
                second_name=second_name,
                last_name=last_name,
                password=password,
                save=False
        )

        user.is_admin = True

        if save:
            user.save(using=self._db)

        return user
