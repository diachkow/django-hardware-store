from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from rolepermissions.checkers import (
    has_permission as check_permission,
    has_object_permission as check_object_permission
)
from rolepermissions.roles import (
    get_user_roles,
    assign_role,
    clear_roles
)
from rolepermissions.permissions import available_perm_status

from users.exceptions import RoleAssignmentException
from users.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    """A project's main user model

    A user is an operator that interacts with system, using it's
    features. There are 3 user roles, defined in `general.roles`
    module.

    Attributes:
        email (str): A user email adress. Used for login as username.
        first_name (str): A user's first name
        second_name (Optional[str]): A user's second name
        last_name (str): A user's last name
        is_active (bool): Logic state - if user can use system
            features
        is_admin (bool): Logic state - if current user is system admin
        role (Optional[str]): A string representation of user role.
    """

    email = models.EmailField(max_length=256, unique=True)
    first_name = models.CharField(max_length=64, blank=False)
    second_name = models.CharField(max_length=64, blank=True)
    last_name = models.CharField(max_length=64, blank=False)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('first_name', 'last_name')

    objects = UserManager()

    @property
    def role(self):
        user_roles = get_user_roles(self)

        if user_roles:
            return user_roles[0].name

    @role.setter
    def role(self, value):
        if value is not None and not isinstance(value, str):
            raise RoleAssignmentException(
                f'A value should be str or None, not {type(value)}'
            )

        if not value:
            clear_roles(self)
            return

        if not self.role:
            assign_role(self, value)
        else:
            clear_roles(self)
            assign_role(self, value)

    # django-role-permissions functions wrapped to be used as
    # user model methods

    @property
    def rolepermissions(self):
        return available_perm_status(self)

    def has_permission(self, permission):
        return check_permission(self, permission)

    def has_object_permission(self, obj, permission):
        return check_object_permission(permission, self, obj)
