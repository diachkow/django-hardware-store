from django import forms

from users.models import User


class UserCreateForm(forms.ModelForm):
    """
    User registration (sign up) form
    """

    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput,
    )
    confirm_password = forms.CharField(
        label='Confirm password',
        widget=forms.PasswordInput,
        help_text='Input the same password as above'
    )

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name')

    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')

        if password != confirm_password:
            raise forms.ValidationError(
                'Password confirmation do not match',
                code='confirm_mismatch'
            )

        return confirm_password

    def save(self):
        return User.objects.create_user(
            email=self.cleaned_data['email'],
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
            second_name=self.cleaned_data.get('second_name', ''),
            password=self.cleaned_data['password'],
            role='shop_assistant'
        )


class UserLoginForm(forms.Form):
    """
    User login form, validates user creadentials
    """

    email = forms.EmailField(label='Email')
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()

        query = User.objects.filter(email=cleaned_data.get('email'))

        if not query.exists():
            raise forms.ValidationError(
                'Invalid email adress',
                code='invalid_email'
            )

        user = query.last()

        if not user.check_password(cleaned_data.get('password')):
            raise forms.ValidationError(
                'Invalid password',
                code='invalid_password'
            )

        return cleaned_data

    def save(self):
        return User.objects.get(email=self.cleaned_data.get('email'))
