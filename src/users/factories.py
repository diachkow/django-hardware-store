import factory

from users.models import User


class UserModelFactory(factory.django.DjangoModelFactory):
    """User Model Factory

    Works as DjangoModelFactory, but refreshes the user data on create to
    fetch the changes were made on post_save signal handling (usually, the user
    roles and groups are set in signal handler).
    """

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        manager = cls._get_manager(model_class)
        if cls._meta.django_get_or_create:
            instance = cls._get_or_create(model_class, *args, **kwargs)
        else:
            instance = manager.create_user(*args, **kwargs)
        return manager.get(id=instance.id)


class UserFactory(UserModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('email',)

    first_name = 'John'
    second_name = 'Joseph'
    last_name = 'Doe'
    email = factory.LazyAttribute(
        lambda o: o.first_name + o.last_name + '@gmail.com'
    )
    password = factory.PostGenerationMethodCall('set_password', '123')

    @factory.post_generation
    def role(obj, create, extracted, **kwargs):
        if extracted:
            obj.role = extracted
