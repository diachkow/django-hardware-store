from django.test import TestCase

from general.roles import ShopAssistant

from users.models import User
from users.exceptions import RoleAssignmentException


class TestUser(TestCase):
    def test_user_creation(self):
        user = User.objects.create_user(
            email='johndoe@gmail.com',
            first_name='John',
            last_name='Doe',
            password='123',
            role='shop_assistant'
        )

        self.assertIsNotNone(user.id)
        self.assertEqual(user.role, 'shop_assistant')

    def test_user_role_setter(self):
        user = User.objects.create_user(
            email='johndoe@gmail.com',
            first_name='John',
            last_name='Doe',
            password='123',
        )

        user.role = 'shop_assistant'

        self.assertEqual(user.role, 'shop_assistant')

    def test_user_role_setter_none_value(self):
        user = User.objects.create_user(
            email='johndoe@gmail.com',
            first_name='John',
            last_name='Doe',
            password='123',
            role='shop_assistant'
        )

        user.role = None
        self.assertIsNone(user.role)

