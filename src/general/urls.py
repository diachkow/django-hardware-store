from django.urls import path

from general.views import IndexView

app_name = 'general'

urlpatterns = [
    path('', IndexView.as_view(), name='index')
]
