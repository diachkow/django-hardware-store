from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    """
    A welcome view, that greets user and provides him a briefly
    introduction into project features
    """

    template_name = 'general/index.html'
